FROM mysql

# Add crontab file in the cron directory
ADD crontab /etc/cron.d/hello-cron

# Give execution rights on the cron job
Run chmod 06644 /etc/cron.d/hello-cron

# Create the log file to be able to run tail
RUN touch /var/log/cron.log

# Install Cron
RUN apt-get update \
&& apt-get install apt-file \
&& apt-file update \
&& apt-get install cron

# Run the command on container startup
CMD cron && tail -f /var/log/cron.log

